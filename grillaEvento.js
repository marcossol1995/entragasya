		function clickableGrid( rows, data, callback ){
				var i=0;
				var grid = document.getElementById(rows);
				grid.className = 'grid';
				if (data.hasOwnProperty('errorMessage') && data.errorMessage.includes("inexistente")){
					dibujarFila(grid,"NO HAY RESULTADOS");
				}
				for (var r=0;r<data.direccionesNormalizadas.length;++r){
					var cell=dibujarFila(grid,data.direccionesNormalizadas[r].direccion);
					i=i++;
					cell.addEventListener('click',(function(el,r,i){
						return function(){
							callback(el,r,i);
						}
					})(cell,r,i),false);
				}
				return grid;
		}

		function dibujarFila(grid,texto){
			var tr = grid.appendChild(document.createElement('tr'));
			var cell = tr.appendChild(document.createElement('td')  );
			cell.innerHTML = texto;
			return cell;
		}