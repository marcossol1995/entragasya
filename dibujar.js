        function dibujarMarker(lat,lon,icon,popup){
            marker = L.marker([lat,lon],{icon}).addTo(mymap);
            marker.bindPopup("<b>"+popup+"</b><br>").openPopup();
            if (icon.options.iconUrl.includes("green")){
                markerClustersgreen.addLayer( marker );
            }else if(icon.options.iconUrl.includes("red")){
                markerClustersred.addLayer( marker );
            }

        }

        function dibujarRecorrido(recorrido,i){   
            var x=i;
            var marker= L.marker([recorrido[x].lat,recorrido[x].lon],{yellowIcon}).addTo(mymap);
            marker.bindPopup("<b>En camino...</b><br>").openPopup();
            setTimeout(function() {
                if(x+1==recorrido.length){
                    alert('Envio finalizado');
                    return;
                }
                setTimeout(function() {
                    marker.remove();
                    dibujarRecorrido(recorrido,x+=1);
                },1000);
            }, 500);
        }