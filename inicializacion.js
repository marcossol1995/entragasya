$(function(){
    $("#butt").click(function(){
        $('.list-group-item-success').remove();
        $('.list-group-item-danger').remove();
        $.get('https://entregasya.herokuapp.com/api/incidents', function(data){//Incidencias
            var i=0;
                    for(x in data.incidents){
                        var incident={id:data['incidents'][i]['id'],lat:data['incidents'][i]['coordinate']['lat'],lon:data['incidents'][i]['coordinate']['lon'],type_id:data['incidents'][i]['type_id'],date:data['incidents'][i]['date']};
                        incidents[i]=incident;
                        i++;
                    }
                
            $.get('https://entregasya.herokuapp.com/api/incidenttypes', function(data){//tipo de incidencia
                var i=0;
                    for(x in data.incidenttypes){
                        var incidenttype={id:data['incidenttypes'][i]['id'],description:data['incidenttypes'][i]['description']};
                        incidenttypes[i]=incidenttype;
                        i++;
                    }
                    inicializarIncidentes(incidents,incidenttypes);
            })
        })

        $.get('https://entregasya.herokuapp.com/api/deliverydrivers', function(data){
                var i=0;
                for(x in data.drivers){
                    var driver={name:data['drivers'][i]['name'], surname:data['drivers'][i]['surname'], id:data['drivers'][i]['id'], lat:0,lon:0,recorrido:null};
                    drivers[i]=driver;
                    i++;
                    }
            $.get('https://entregasya.herokuapp.com/api/requests/', function(data){
                if(solicitud==1 || solicitud==0){
                    var i=0;
                    var x=0;
                    if(solicitud==1){
                        i=3;
                    }
                    for(i;i<drivers.length;i++){

                            drivers[i].lat=data.requests[solicitud].availableDrivers[x].position.lat;
                            drivers[i].lon=data.requests[solicitud].availableDrivers[x].position.lon;
                            dibujarMarker(drivers[i].lat,drivers[i].lon,greenIcon,drivers[i].name);
                            agregarALista(drivers[i].name,'repartidores',drivers[i].id)
                            x++;
                            if(i==2){
                                break;
                            }   
                    }
                    dibujarMarker(data['requests'][solicitud]['sender']['lat'],data['requests'][solicitud]['sender']['lon'],blueIcon,'Origen');
                    dibujarMarker(data['requests'][solicitud]['receiver']['lat'],data['requests'][solicitud]['receiver']['lon'],blueIcon,'Destino');
                }
            })
        })  
    })
})

function inicializarIncidentes(incidents,incidenttypes){
    var i=0
    for(x in incidents){
       
        for(n=0;n<incidenttypes.length;n++){
            if(incidenttypes[n].id==incidents[i].type_id){
                dibujarMarker(incidents[i].lat,incidents[i].lon,redIcon,incidenttypes[n].description);
                agregarALista(incidenttypes[n].description,'incidencias',0);
            }
        }
    i++;
    }
}